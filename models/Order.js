const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    orders: [{
        totalAmount: { type: Number, required: [true, "Total is required."] },

        purchasedOn: { type: Date, default: new Date() },

        userID: { type: String, required: [true, "User ID is required."] },

        productID: { type: String, required: [true, "Product ID is required."] },

        orders: { type: String, required: [true, "Orders are required."] },
    }, ],
});

module.exports = mongoose.model("Order", orderSchema);