const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First Name is required."],
    },

    lastName: {
        type: String,
        required: [true, "Last Name is required."],
    },

    email: {
        type: String,
        required: [true, "EMAIL is required."],
    },

    password: {
        type: String,
        required: [true, "Password is required."],
    },
    activeStatus: {
        type: Boolean,
        default: true,
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
});

module.exports = mongoose.model("User", userSchema);