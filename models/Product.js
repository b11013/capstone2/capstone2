const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    softwareName: {
        type: String,
        required: [true, "Software name is required"],
    },

    description: {
        type: String,
        required: [true, "Software description is required"],
    },

    price: {
        type: Number,
        required: [true, "Price is required"],
    },

    category: {
        type: String,
        required: [true, "Product category is required"],
    },

    isActive: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: new Date(),
    },
});

module.exports = mongoose.model("Product", productSchema);