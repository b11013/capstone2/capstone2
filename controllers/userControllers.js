const User = require("../models/User");
const bcrypt = require("bcryptjs");
const auth = require("../auth");

//REGISTER USER
module.exports.registerUser = (req, res) => {
    req.body;
    let passwordInput = req.body.password;
    const hashedPW = bcrypt.hashSync(passwordInput, 10);

    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPW,
    });

    User.findOne({ email: req.body.email })
        .then((result) => {
            if (result !== null && result.email === req.body.email) {
                return res.send("Email is already registered!");
            } else {
                newUser
                    .save()
                    .then((user) => res.send("User registered successfully! " + user));
            }
        })
        .catch((error) => res.send(error));
};

//VIEW All Users
module.exports.getAllUsers = (req, res) => {
    User.find({})
        .then((result) =>
            res.send("Here are all the registered users: \n" + result)
        )
        .catch((err) => res.send(err));
};

//LOGIN User
module.exports.loginUser = (req, res) => {
    User.findOne({ email: req.body.email })
        .then((foundUser) => {
            if (foundUser === null) {
                return res.send("User not found in the database.");
            } else {
                const isPasswordCorrect = bcrypt.compareSync(
                    req.body.password,
                    foundUser.password
                );

                if (isPasswordCorrect) {
                    return res.send({
                        "Login Successful! Here is your Access Token:": auth.createAccessToken(foundUser),
                    });
                } else {
                    return res.send("Incorrect Password, please try again!");
                }
            }
        })
        .catch((err) => res.send(err));
};

// Update first name and last name
module.exports.updateUserDetails = (req, res) => {
    let updates = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
    };
    User.findByIdAndUpdate(req.user.id, updates, { new: true })
        .then((updatedUserInfo) =>
            res.send(
                req.body.firstName + "'s name updated successfully!" + updatedUserInfo
            )
        )
        .catch((err) => res.send(err));
};

//SET an ADMIN
module.exports.updateAdmin = (req, res) => {
    let updates = {
        isAdmin: true,
    };
    User.findByIdAndUpdate(req.params.id, updates, { new: true })

    .then((updatedAdmin) =>
            res.send("Successfully set as Admin!\n" + updatedAdmin)
        )
        .catch((err) => res.send(err));
};