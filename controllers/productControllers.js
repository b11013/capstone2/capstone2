const Product = require("../models/Product");
const bcrypt = require("bcryptjs");
const auth = require("../auth");

// Add Product
module.exports.addProduct = (req, res) => {
    req.body;

    let newProduct = new Product({
        softwareName: req.body.softwareName,
        description: req.body.description,
        price: req.body.price,
        category: req.body.category,
    });
    Product.findOne({ softwareName: req.body.softwareName })
        .then((result) => {
            if (result !== null && result.softwareName === req.body.softwareName) {
                return res.send("Product Already Registered!");
            } else {
                newProduct
                    .save()
                    .then((product) =>
                        res.send("Product registered successfully! " + product)
                    );
            }
        })
        .catch((error) => res.send(error));
};

//view all products
module.exports.getAllProducts = (req, res) => {
    Product.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//view single product
module.exports.getSingleProduct = (req, res) => {
    Product.findById(req.params.id)
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

//Edit a Product Info
module.exports.editProduct = (req, res) => {
    let updates = {
        softwareName: req.body.softwareName,
        description: req.body.description,
        price: req.body.price,
        category: req.body.category,
    };

    Product.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((result) =>
            res.send(req.body.softwareName + " edited successfully. " + result)
        )
        .catch((error) => res.send(error));
};

//ARCHIVE A Product
module.exports.archiveCourse = (req, res) => {
    let archive = {
        isActive: false,
    };

    Product.findByIdAndUpdate(req.params.id, archive, { new: true })
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

//ACTIVATE A Product
module.exports.activateProduct = (req, res) => {
    let activate = {
        isActive: true,
    };

    Product.findByIdAndUpdate(req.params.id, activate, { new: true })
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

//View All Active Product
module.exports.viewActiveProduct = (req, res) => {
    Product.find({ isActive: true })
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//View Product per category
module.exports.viewPerCategory = (req, res) => {
    Product.find({ category: req.body.category })
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};