const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();
const port = 6000;

//db connection
mongoose.connect(
    "mongodb+srv://admin:admin@capstone2.8kmft.mongodb.net/softwareAPI?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Database connection failed."));
db.once("open", () => console.log("Database connection is established."));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

//group routing
const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

const productRoutes = require("./routes/productRoutes");
app.use("/products", productRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`));